(function() {
  'use strict';

  angular
    .module('directors')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('director', {
        url: '/director/:id',
        templateUrl: 'app/main/director/director.html',
        controller: 'DirectorController',
        controllerAs: 'director'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
