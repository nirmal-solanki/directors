(function() {
  'use strict';

  angular
    .module('directors')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
