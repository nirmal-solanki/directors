/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('directors')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
