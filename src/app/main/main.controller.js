(function() {
  'use strict';

  angular
    .module('directors')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, Directors, $state) {

    $scope.isProgress = true;

    $scope.gridOptions = {
      enableSorting: true,
      enableRowSelection: true,
      enableRowHeaderSelection: false,
      columnDefs: [
        { name:'name', field: 'name' },
        { name:'dob', field: 'dob', cellFilter: 'date: "dd-MM-yyyy"'},
        { name:'nationality', field: 'nationality' },
        { name:'city', field: 'city'}
      ]
    };
    $scope.gridOptions.multiSelect = false;
    $scope.gridOptions.modifierKeysToMultiSelect = false;
    $scope.gridOptions.onRegisterApi = function(gridApi){
      //set gridApi on scope
      $scope.gridApi = gridApi;
      gridApi.selection.on.rowSelectionChanged($scope,function(row){
        $state.go('director',{id:row.entity.id});
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){

      });
    };

    $scope.fnListDirectors = function(){
      $scope.isProgress = true;
      Directors.list().then(function(res){
        $scope.isProgress = false;
        $scope.gridOptions.data = res.data;
      })
    };

    $scope.fnInitDirectors = function(){
      $scope.fnListDirectors();
    };

  }
})();
