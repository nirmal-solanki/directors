(function() {
  'use strict';

  angular
    .module('directors')
    .controller('DirectorController', DirectorController);

  /** @ngInject */
  function DirectorController($scope, Directors, $stateParams, $filter) {

    $scope.isProgress = true;
    $scope.fnReturnValue = function(key, value){
      if(key === 'sex'){
        return value === 'M' ? 'Male' : 'Female';
      }else if(key === 'dob'){
        return $filter('date')(value, "dd/MM/yyyy");
      }else{
        return value;
      }
    };
    $scope.fnGetDirector = function(id){
      $scope.isProgress = true;
      Directors.get(id).then(function(res){
        $scope.director = res;
        if(Object.keys($scope.director).length){
          $scope.isProgress = false;
        }
      })
    };

    $scope.fnInitDirector = function(){
      $scope.fnGetDirector($stateParams.id);
    };

  }
})();
