(function() {
  'use strict';

  angular
    .module('directors')
    .factory('Directors', Directors);

  /** @ngInject */
  function Directors($q, $http, toastr) {
    var Directors = {};

    Directors.list = function () {
      var defer = $q.defer();
      $http.get("app/main/directors.json")
        .then(function(responce){
          defer.resolve(responce);
        },function(error){
          toastr.error(error);
          defer.resolve([]);
        });
      return defer.promise;
    };

    Directors.get = function (id) {
      var defer = $q.defer();
      $http.get("app/main/directors.json")
        .then(function(responce){
          var index = responce.data.map(function(obj){return obj.id}).indexOf(parseInt(id));
          if(index != -1){
            defer.resolve(responce.data[index]);
          }else{
            toastr.error('Record not found');
            defer.resolve({});
          }
        },function(error){
          toastr.error(error);
          defer.resolve([]);
        });
      return defer.promise;
    };

    return Directors;
  }
})();
