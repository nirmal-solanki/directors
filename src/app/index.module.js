(function() {
  'use strict';

  angular
    .module('directors', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize',
      'ngMessages', 'ngAria', 'ui.router', 'ui.bootstrap', 'toastr',
      'ui.grid', 'ui.grid.selection']);

})();
